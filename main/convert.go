package main

import (
	"strconv"
)

// IntToString converts integer to string
func IntToString(i int) string {
	return strconv.Itoa(i)
}

// Int32ToFloat32 converts integer32 to float32
func Int32ToFloat32(i int32) float32 {
	return float32(i)
}

//Int64ToFloat64 converts integer64 to float64
func Int64ToFloat64(i int64) float64 {
	return float64(i)
}

//StringToBool converts string to boolean
func StringToBool(s string) (bool, error) {
	return strconv.ParseBool(s)
}

//StringToInt converts string to integer
func StringToInt(s string) (int, error) {
	return strconv.Atoi(s)
}

//RuneToString converts rune to string
func RuneToString(r rune) string {
	return string(r)
}

//ByteToInteger converts byte to integer
func ByteToInteger(b byte) (int, error) {
	return strconv.Atoi(string(b))
}

//ByteToString converts byte to string
func ByteToString(b byte) string {
	return string(b)
}
