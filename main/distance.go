package main

import "regexp"

func stringDistance(str1, str2 string) int {
	var i, distance int
	re := regexp.MustCompile(`^[ACGT]+$`)
	if re.MatchString(str1) && re.MatchString(str2) {
		//Check different lengths
		if len(str1) == len(str2) {
			for _, v := range str1 {
				if RuneToString(v) != ByteToString(str2[i]) {
					distance++
				}
				i++
			}
			return distance
		}
		//if DNA strand have different lengths return -2
		return -2
	}
	//if one or both not a DNA strand return -3
	return -3
}
